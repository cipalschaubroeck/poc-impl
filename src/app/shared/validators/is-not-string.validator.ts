import { FormControl, ValidatorFn } from '@angular/forms';

export function createIsNotStringValidator(): ValidatorFn {
    // Deze functie geeft "null" terug indien valid, of een object indien invalid
    return function validateNotAString(c: FormControl) {
        const val = c.value;

        // Bij een lege waarde geven we "valid" terug omdat we dit willen laten opvangen door de required-validator
        if (val === undefined || val === null) {
            return undefined;
        }
        if ((typeof val) === 'string') {
            return {
                notAString: {given: val, valid: false}
            };
        }

        return undefined;
    };
}
