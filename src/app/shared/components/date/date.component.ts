import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-date',
    templateUrl: './date.component.html',
    styleUrls: ['./date.component.scss'],
    // Onze ControlValueAccessor registreren
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DateComponent),
            multi: true
        }
    ]
})
export class DateComponent implements OnInit, ControlValueAccessor {

    _dateValue: Date;

    @Input()
    showTime: boolean = false;

    @Input()
    showSeconds: boolean = false;

    @Input()
    monthNavigator: boolean = true;

    @Input()
    yearNavigator: boolean = true;

    nl: any;

    // Lege methode voorzien om component 'dirty' of 'touched' te markeren (deze methode wordt later overschreven)
    private _onTouchedCallback: () => {};

    // Lege methode voorzien om changes door te geven die later overschreven wordt
    propagateChange = (_: any) => {
    };

    constructor() {
    }

    ngOnInit() {
        this.nl = {
            firstDayOfWeek: 1,
            dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
            dayNamesShort: ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vr', 'Za'],
            dayNamesMin: ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vr', 'Za'],
            monthNames: ['Januari', 'Februari', 'Maart', 'April',
                'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Maa', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        };
    }

    get dateValue() {
        return this._dateValue;
    }

    set dateValue(val: Date) {
        this._dateValue = val;
        this.propagateChange(this._dateValue);
        this._onTouchedCallback();
    }

    // method that writes a new value from the form model into the view
    writeValue(value: any) {
        this._dateValue = value;
    }

    // method that registers a handler that should be called when something in the view has changed
    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    // this method registers a handler specifically for when a control receives a touch event
    registerOnTouched(fn) {
        this._onTouchedCallback = fn;
    }

}
