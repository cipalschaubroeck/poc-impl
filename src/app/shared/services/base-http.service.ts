import { Injectable } from '@angular/core';

/**
 * This service can be us
 */
@Injectable()
export abstract class BaseHttpService {

  protected API_BASE: string;

  constructor() {
    // TODO this can be read from a properties file depending on your application profile.
    // TODO this can also have an impact on the http-content-interceptor
    this.API_BASE = '/api';
  }

}
