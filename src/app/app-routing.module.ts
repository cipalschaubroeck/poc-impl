import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { SettingsComponent } from './settings/settings.component';
import { DocManagementShellComponent } from './docManagement/presentation/doc-management-shell/doc-management-shell.component';


const routes: Routes = [
  { path: "", redirectTo: "/docManagement", pathMatch: "full" },
  { path: "settings", component: SettingsComponent},
 // { path: 'docManagement', loadChildren: 'app/docManagement/docManagement.module#DocManagementModule'}
  { path: 'docManagement', component: DocManagementShellComponent}
];

export const appRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
