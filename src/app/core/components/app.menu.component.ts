import { Component, forwardRef, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-menu',
  templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit, OnDestroy {

  @Input() reset: boolean;

  model: any[];

  subscription: Subscription;

  //categoryPipe: CategoryPipe = new CategoryPipe();

  constructor(@Inject(forwardRef(() => AppComponent)) public app: AppComponent) {
    // menu-items voor admin tonen of verbergen als admin instellingen gewijzigd zijn

  }

  ngOnInit() {
   // const isAdmin = this.userService.isAdmin();

    this.model = [
      {label: 'Productcatalogus'},
      {
        label: 'Mijn bestellingen',
        routerLink: ['/bestellingen/mijn-bestellingen'],
        visible: true
      },
      {
        label: 'Administratie',
        items: [
          {label: 'Leveranciers', routerLink: ['/leveranciers/administratie']},
          {label: 'Producten', routerLink: ['/producten/administratie']},
          {label: 'Klanten', routerLink: ['/klanten/administratie']},
          {label: 'Bestellingen', routerLink: ['/bestellingen/administratie']}
        ],
        visible: true
      },
      {
        label: 'Rapporten',
        items: [
          {
            label: 'Uitgaveprofiel',
            routerLink: ['/rapporten/klant-uitgaveprofiel']
          },
          {
            label: 'Top 10 producten',
            routerLink: ['rapporten/product-meest-verkocht']
          },
          {label: 'Terugroepactie', routerLink: ['rapporten/terugroepactie']},
          {
            label: 'Populariteit product',
            routerLink: ['/rapporten/product-populariteit']
          }
        ],
        visible: true
      },
      {
        label: 'Guni²',
        items: [
          {
            label: 'Styleguide',
            routerLink: ['/guni-square/styleguide']
          }
        ]
      }
    ];

    // Dynamische menu-items voor categorieën opbouwen
    const categories = [];
/*    for (const item in Category) {
      // Alle enum-waardes in Category overlopen (aangezien
      // ook de getallen hierin zitten, moeten we enkel de strings in de lijst toevoegen)
      if (Category.hasOwnProperty(item) && !/^\d+$/.test(item)) {
        categories.push({
          label: this.categoryPipe.transform(item, undefined),
          routerLink: ['/producten/catalogus', item]
        });
      }
    }*/
    this.model[0].items = categories;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  changeTheme(theme: string) {
    const themeLink: HTMLLinkElement = document.getElementById('theme-css') as HTMLLinkElement;
    const layoutLink: HTMLLinkElement = document.getElementById('layout-css') as HTMLLinkElement;

    themeLink.href = `assets/theme/theme-${theme}.css`;
    layoutLink.href = `assets/layout/css/layout-${theme}.css`;
  }
}
