import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { MenuItem } from 'primeng/primeng';

@Injectable()
export class BreadcrumbService {

  private itemsSource: Subject<MenuItem[]> = new Subject<MenuItem[]>();

  private _itemsHandler: Observable<MenuItem[]> = this.itemsSource.asObservable();

  setItems(items: MenuItem[]) {
    this.itemsSource.next(items);
  }

  get itemsHandler(): Observable<MenuItem[]> {
    return this._itemsHandler;
  }

}
