import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DocumentProcess } from '../../shared/models/documentProcess.model'
import { DocumentProcessType } from '../../shared/models/documentProcessType.model';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit {

  @Input() cols: any[];
  @Input() errorMessage: string;
  @Input() documentProcessList: DocumentProcess[];
  @Input() selectedDocumentProcess: DocumentProcess;

  @Output() selected = new EventEmitter<DocumentProcess>();
  @Output() unselected = new EventEmitter<DocumentProcess>();
  constructor() { }

  ngOnInit() {
    this.cols = [
      { field: 'creationDate', header: 'Date' },
      { field: 'type', header: 'Document process type' },
      { field: 'status', header: 'Status' }
    ];
  }

  isDocumentProcessType(pet: DocumentProcessType): pet is DocumentProcessType {
    return (<DocumentProcessType>pet).name !== undefined;
  }


  onRowSelect(event) {
    this.selected.emit(event);
    console.log('onRowSelect');
  }

  onRowUnselect(event) {
    this.selected.emit(event);
  }
}
