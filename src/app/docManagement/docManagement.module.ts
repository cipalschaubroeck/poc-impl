import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DocManagementShellComponent } from './presentation/doc-management-shell/doc-management-shell.component';
import { LeftPanelComponent } from './components/left-panel/left-panel.component';
import { RightPanelComponent } from './components/right-panel/right-panel.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducers/docManagement.reducer';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature('docManagement', reducer)
  ],
  declarations: [
    DocManagementShellComponent,
    LeftPanelComponent,
    RightPanelComponent
  ]
})
export class DocManagementModule { }
