import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocManagementShellComponent } from './presentation/doc-management-shell/doc-management-shell.component';

const routes: Routes = [
  {path: '', component: DocManagementShellComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DocManagementRoutingModule {

  constructor() {
  }

}
