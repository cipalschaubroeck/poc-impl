import { DocumentProcessActions, DocumentProcessActionTypes } from '../actions/docManagement.action';
import { DocManagementState } from '../../shared/models/docManagementState.model';
import { DocumentProcessType } from '../../shared/models/documentProcessType.model';
import { DocumentProcess } from '../../shared/models/documentProcess.model';
import { TypeName } from '../../shared/enums/documentProcess.enum';

const emailtype: DocumentProcessType = {
   'tabs':'dede',
   'name': TypeName.EMAIL,
   'documentBody':'ded',
   'templateName':'ded'
};
//Only for testing mockup
const  initlistProcess : DocumentProcess[] = [{
  'id': 1,
  'street': 'calle 1',
  'type': emailtype,
  'creationDate': 'fecha1',
  'status': 'ready',
  'documentHandler': 'pdf',
  'Attachments': '/dewd/wded'
}];


const initialState: DocManagementState = {
  currentDocumentProcessId: null,
  documentProcessList: initlistProcess,
  error: ''
};

export function reducer(state = initialState, action: DocumentProcessActions): DocManagementState {
  switch (action.type) {
    case DocumentProcessActionTypes.SetCurrentDocumentProcess:
      // @ts-ignore
      console.log('SetCurrentDocumentProcess',action.payload.data.id);
      return {
        ...state,
        // @ts-ignore
        currentDocumentProcessId: action.payload.data.id
      };
    default:
      return state;
  }

}
