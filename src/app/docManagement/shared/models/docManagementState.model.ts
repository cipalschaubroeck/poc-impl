// State for this feature (DocManagement)
import { DocumentProcess } from './documentProcess.model';

export interface DocManagementState {
  currentDocumentProcessId: number | null;
  documentProcessList: DocumentProcess[];
  error: string;
}
